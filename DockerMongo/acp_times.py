"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

BREVET_DICT = {200: [15, 34], 300: [15, 32], 400: [15, 30], 600: [11.428, 28], 1000: [13.333, 26]}

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    start_time = 0
    base_hours = 0
    base_minutes = 0
    brevet_multiple = brevet_dist_km * 1.20
    if control_dist_km > brevet_multiple:
        return False
    if brevet_dist_km < control_dist_km and control_dist_km <= brevet_multiple:
        control_dist_km = brevet_dist_km

    for x, max_speed in sorted(BREVET_DICT.items()):
        if control_dist_km <= x:
            start_time += control_dist_km / max_speed[1]
            break
        elif control_dist_km > x:
            start_time += 200 / max_speed[1]
        control_dist_km = control_dist_km - 200

    base_hours = math.floor(start_time)
    base_minutes = round((start_time - base_hours) * 60)
    return arrow.get(brevet_start_time).shift(hours=+base_hours, minutes=+base_minutes).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
        control_dist_km:  number, the control distance in kilometers
        brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
        brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    a_point = control_dist_km
    start_time = 0
    base_hours = 0
    base_minutes = 0
    brevet_multiple = brevet_dist_km * 1.20
    if control_dist_km == 0:
        return arrow.get(brevet_start_time).shift(hours=+1).isoformat()
    if control_dist_km > brevet_multiple:
        return False
    if brevet_dist_km < control_dist_km and control_dist_km <= brevet_multiple:
        control_dist_km = brevet_dist_km
        a_point = control_dist_km

    for x, min_speed in sorted(BREVET_DICT.items()):
        if a_point <= x:
            start_time += a_point / min_speed[0]
            break
        elif a_point > x:
            start_time += 200 / min_speed[0]
        a_point = a_point - 200

    base_hours = math.floor(start_time)
    base_minutes += round((start_time - base_hours) * 60)
    
    if control_dist_km <= 59:
        to_add = 60 - control_dist_km
        base_minutes += to_add
        if base_minutes > 60:
            base_hours += 1
            base_minutes -= 60

    if control_dist_km == 200:
        base_minutes += 10
    return arrow.get(brevet_start_time).shift(hours=+base_hours, minutes=+base_minutes).isoformat()
