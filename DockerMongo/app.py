import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow
import acp_times
import config
import logging
import flask

app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

db.tododb.delete_many({})
@app.route('/')
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route('/invalidsubmit')
def submiterror():
    return render_template('invalidsubmit.html')

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    app.logger.debug("request.args: {}".format(request.args))
    km = request.args.get('km', 0, type=int)
    brevet_dist = request.args.get('brevet_dist', 0, type=int)
    brevet_start = request.args.get('brevet_start') or arrow.now().isoformat()
    open_time = acp_times.open_time(km, brevet_dist, brevet_start)
    close_time = acp_times.close_time(km, brevet_dist, brevet_start)

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route('/display', methods=['POST'])
def display():
   
    _items = db.tododb.find()
    items = [item for item in _items]
    if (len(items) == 0): 
        return render_template('invalidsubmit.html')

    return render_template('display.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    open_list = []
    close_list = []
    km_list = []    

    open_data = request.form.getlist("open")
    close_data = request.form.getlist("close")
    km_data = request.form.getlist("km")
  
    for item in open_data:
        if str(item) != '': open_list.append(str(item))
    for item in close_data:
        if str(item) != '': close_list.append(str(item))
    for item in km_data:
        if str(item) != '': km_list.append(str(item))
	
    if(len(open_list) == 0):
        return redirect(url_for('invalidsubmit'))    

    for x in range(length):
        item_doc = {'open_times': open_list[x], 'close_times': close_list[x], 'km_distances': km_list[x]}
        db.tododb.insert_one(item_doc)

    return redirect(url_for('index'))

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
