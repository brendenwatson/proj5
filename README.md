##READ ME
Author: Brenden Watson
Contact: brendenw@uoregon.edu
This project uses Flask and Ajax to calculate and display brevet control points and times in a brevet race much like the calculator here (https://rusa.org/octime_acp.html). ACP standard is used for the opening and closing times of each control point. The calculator can be used on localhost. 

Functionality was added in project 5 to add submit and display keys. The program now utilizes MongoDB to store controle points in a database then display those controle points with the new display button. 